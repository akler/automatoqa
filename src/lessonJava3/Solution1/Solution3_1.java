package lessonJava3.Solution1;

public class Solution3_1{
    /*
Задача 1.
Есть 2 кошки и 2 собаки.
 1 кошка и 1 кот обладают именем, хозяином и местом жительства.
 1 собака бездомная, но с гордым именем.
 1 собака домашняя, но без имени.
Все животные могут бегать, есть и издавать некий голос
Реализуйте эту ситуацию с использованием Java и ООП.
     */
    public static void main(String[] args){

        Cat nissa = new Cat("Кошка", "Нисса", "Тенистый проезд", "Вова");
        Cat dhus = new Cat("Кот", "Дхуся", "Серпухов", "Ари");
        Dog seymour = new Dog("Сэймур",null);
        Dog sparky = new Dog(null, "3-я улица Строителей, кв.12");

        Animal[] pets = {nissa, dhus, seymour, sparky};
        for (int i=0;i<pets.length;i++){
            System.out.println(pets[i].name);
            Animal.run();
            Animal.eat();
            if (pets[i] instanceof Dog){
                Dog.bark();
            }
            else {
                Cat.bark();
            }
            System.out.println("=========");
        }

    }
}
