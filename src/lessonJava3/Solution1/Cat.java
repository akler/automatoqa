package lessonJava3.Solution1;

public class Cat extends Animal{
    String homePlace;
    String master;

    Cat(String gender, String name, String homePlace, String master){
        super(gender, name);
        this.homePlace = homePlace;
        this.master = master;
    }

    public static void bark(){
        System.out.println("Миииияу!");
    }

}
