package lessonJava3.Solution1;

public class Dog extends Animal{
    String homePlace;

    Dog(String name, String homePlace){
        this.name = name;
        this.homePlace = homePlace;
    }

    public static void bark(){
        System.out.println("Вуф-Вуф!");
    }
}
