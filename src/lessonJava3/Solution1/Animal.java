package lessonJava3.Solution1;

class Animal{
    String gender;
    String name;

    Animal(){
    }

    Animal(String gender){
        this.gender = gender;
    }

    Animal(String gender, String name){
        this.gender = gender;
        this.name = name;
    }

    public static void run(){
        System.out.println("Я бегу как животное");
    }

    public static void eat(){
        System.out.println("Омном-ном!");
    }

    public static void bark(){
        System.out.println("Голос!");
    }
}
