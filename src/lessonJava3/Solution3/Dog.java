package lessonJava3.Solution3;

public class Dog extends Animal {
    String homePlace;
    public double health = 7;
    public double strength = 1.2;

    Dog(String name) {
        super(name);
    }

    Dog(String name, String homePlace) {
        this.name = name;
        this.homePlace = homePlace;
    }

    @Override
    public double getHealth() {
        return health;
    }

    @Override
    public double getStrength() {
        return strength;
    }

    public static void bark(){
        System.out.println("Вуф-Вуф!");
    }
}
