package lessonJava3.Solution3;

public class Cat extends Animal {
    String homePlace;
    String master;
    public double health = 9;
    public double strength = 1.0;

    Cat(String gender, String name, String homePlace, String master) {
        super(gender, name);
        this.homePlace = homePlace;
        this.master = master;
    }

    @Override
    public double getHealth() {
        return health;
    }

    @Override
    public double getStrength() {
        return strength;
    }

    public static void bark() {
        System.out.println("Миииияу!");
    }

}
