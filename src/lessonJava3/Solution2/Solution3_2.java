package lessonJava3.Solution2;

public class Solution3_2{
    /*
Задача 2.Кошки из прошлого задания не поделили территорию. Реализуйте программу «кошачьи бои».
     */
    public static void main(String[] args){

        Cat nissa = new Cat("Кошка", "Нисса", "Тенистый проезд", "Вова");
        Cat dhus = new Cat("Кот", "Дхуся", "Серпухов", "Ари");
        Dog seymour = new Dog("Сэймур",null);
        Dog sparky = new Dog(null, "3-я улица Строителей, кв.12");

        System.out.println("Добро пожаловать на вечернее шоу «Кошачьи бои»!");
        System.out.println();
        nissa.fighting(nissa);
        System.out.println("=====");
        nissa.fighting(dhus);
        System.out.println("=====");
        dhus.fighting(nissa);
        System.out.println("=====");
        dhus.fighting(dhus);
    }
}
