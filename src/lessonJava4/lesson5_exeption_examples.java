package lessonJava4;

import java.io.BufferedInputStream;
import java.io.*;
import java.io.FileInputStream;

public class lesson5_exeption_examples {
    public static void main(String[] args){
        try {
            dogHouse();
            BufferedInputStream stream = new BufferedInputStream(new FileInputStream(new File("")));
        }
        catch (FileNotFoundException e){
            System.out.println(e + " Файл не найден");
        }
        catch (Exception e){
            System.out.println(e + " Файл не найден, но это не точно");
        }
        finally {
            System.out.println("Finally отрабатывает в любом случае");
        }
    }
    public static void dogHouse() throws NullPointerException{
        //throw new Exception();
    }
}
