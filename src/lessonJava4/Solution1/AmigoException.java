package lessonJava4.Solution1;

public class AmigoException extends Exception {

    public AmigoException(String s) {
        super(s);
    }
}
