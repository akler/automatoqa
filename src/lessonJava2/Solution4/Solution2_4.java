package lessonJava2.Solution4;

import java.util.Arrays;

public class Solution2_4 {
    /*
Задача 4. У вас есть строка “Я у мамы программист”
Необходимо написать программу, которая приводит каждую уникальную букву к строчному регистру и помещает ее в массив.
Все буквы должны находиться в массиве в алфавитном порядке.
Пример работы программы для строки “Hello world”:
    [‘d’,’e’,’h’,’l’,’o’,’r’,’w’]
     */
    public static void main(String[] args) {

        String ANSI_BLUE = "\u001B[34m";
        String input = "Я у мамы программист";
        String modify = "";
        String forprint = "";

        for (int i=0;i<input.length();i++) {
            String extract = input.toLowerCase()
                                  .substring(i,i+1);
            if (modify.contains(extract) || extract.equals(" ")) {
                continue;
            }
            else
                modify = modify.concat(extract);
        }

        char[] output = modify.toCharArray();
        Arrays.sort(output);

        for (int i=0;i<output.length;i++) {
            if (i==output.length-1) {
                forprint = forprint.concat("'" + Character.toString(output[i]) + "'");
            }
            else {
                forprint = forprint.concat("'" + Character.toString(output[i]) + "',");
            }
        }

        System.out.printf(ANSI_BLUE + "[%s]", forprint);

    }
}
