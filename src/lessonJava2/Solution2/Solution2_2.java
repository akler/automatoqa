package lessonJava2.Solution2;

public class Solution2_2 {
    /*
    Задача 1. У вас есть строка “Рассерженный Родион Романович раскричался: «Рано радовались, размокший ремень рюкзака
    разорвался” Необходимо заменить в данной строке все буквы “р” на символ “*”,  используя цикл.
     */
    public static void main(String[] args){

        String before = "Рассерженный Родион Романович раскричался: «Рано радовались, размокший ремень рюкзака разорвался";
        String symbol = "р";
        String after = "";

        for (int i=0;i<before.length();i++){
            String extract = before.substring(i,i+1);
            if (extract.compareToIgnoreCase(symbol) == 0){
                after += "*";
            }
            else
                after += extract;
        }

        System.out.println(after);
    }
}