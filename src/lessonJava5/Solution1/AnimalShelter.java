package lessonJava5.Solution1;

import java.util.*;

public class AnimalShelter {

    static Integer animalCount = 0;

    public static Integer getAnimalCount() {
        return animalCount;
    }

    public static void setAnimalCount(Integer animalCount) {
        AnimalShelter.animalCount = animalCount;
    }

    public static void deposit(Map animalShelter) {
        System.out.println("Кого вы хотите сдать в приют:");
        System.out.println("кошку или собаку?");
        Scanner scanner = new Scanner(System.in);
        String opinion = scanner.nextLine();
        if (opinion.toLowerCase().equals("кошку")) {
            System.out.println("Как зовут котика?");
            String name = scanner.nextLine();
            System.out.println("Что за порода?");
            String breed = scanner.nextLine();
            System.out.println("Цвет окраса?");
            String color = scanner.nextLine();
            System.out.println("Мальчик или девочка?");
            String gender = scanner.nextLine();
            System.out.println("Сколько лет?");
            Integer age = scanner.nextInt();
            Cat cat = new Cat(age,name,breed,color,gender);
            animalShelter.put(++animalCount, cat);
        }
        else if (opinion.toLowerCase().equals("собаку")) {
            System.out.println("Как зовут собаку?");
            String name = scanner.nextLine();
            System.out.println("Что за порода?");
            String breed = scanner.nextLine();
            System.out.println("Цвет окраса?");
            String color = scanner.nextLine();
            System.out.println("Мальчик или девочка?");
            String gender = scanner.nextLine();
            System.out.println("Сколько лет?");
            Integer age = scanner.nextInt();
            Dog dog = new Dog(age,name,breed,color,gender);
            animalShelter.put(++animalCount, dog);
        }
        else {
            System.out.println("Сорян, наш приют принимает только кошек и собак ={");
        }
    }

    public static void withdraw(Map animalShelter) {
        int shelterSize = animalShelter.size();
        Random random = new Random();
        Integer withdrawAnimal = random.nextInt(shelterSize) + 1;

        System.out.println("Добрые люди забрали зверушку под номером " + withdrawAnimal);
        System.out.println("Этим животным оказался/-лась " + animalShelter.get(withdrawAnimal));
        animalShelter.remove(withdrawAnimal);
    }
}
