package lessonJava5.Solution1;

import java.util.*;

/*
1. Создать приют для бездомных кошек и собак. Должна быть реализована возможность добавлять животных в приют а также
возможность взять из приюта случайное животное;

2*. Добавить возможность забирать из приюта животное с
определёнными свойствами («Мама, хочу рыжую собачку!»).
 */
public class Solution5_1 {
    public static void main(String[] args) {

        Map<Integer, Animal> animalShelter = new HashMap<>();
//        Наполняем приют разными тварями
        Cat cat = new Cat(3, "Мурлыка", "Персидская","черепаховый","девочка");
        Integer animalCount = AnimalShelter.getAnimalCount();
        animalShelter.put(++animalCount, cat);
        AnimalShelter.setAnimalCount(animalCount);

        Dog dog = new Dog(22,"Коржик","Шпиц","серый","мальчик");
        animalCount = AnimalShelter.getAnimalCount();
        animalShelter.put(++animalCount, dog);
        AnimalShelter.setAnimalCount(animalCount);



//        Сдаём животинку в приют
        AnimalShelter.deposit(animalShelter);
//        Смотрим, что животинка попала в приют
        System.out.println("Сейчас в приюте " + animalShelter);
//        Забираем рандомную животинку из приюта
        AnimalShelter.withdraw(animalShelter);
//        Смотрим, что животинки больше нет в приюте
        System.out.println("В приюте остались: " + animalShelter);
    }
}
