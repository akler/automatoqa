public class lesson2_cycle_examples {
    public static void main(String[] args)
    {
        int a = 2;
        do {
            System.out.println("In cycle");
            a++;
        }
        while (2+2==a);

        //
        for (int b = 0; b!=2; b++)
        {System.out.println(b);}
        System.out.println("========");

        //Тренируем циклы
        int x = 10;
        int y = 1;
        do {
            System.out.println(y);
            y++;
        }
        while (y<=x);
    }
}
