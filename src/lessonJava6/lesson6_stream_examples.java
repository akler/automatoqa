package lessonJava6;

import java.util.List;
import java.util.Arrays;

public class lesson6_stream_examples {

    public static void main (String[] args) {

        Animal animal = new Animal() {
            @Override
            protected void voice() {
                System.out.println("Голос");
            }
        };
        animal.voice();

//        Animal anotherAnimal = () -> { System.out.println("Woof"); };
//        not working without func.interface

        List<String> myList = Arrays.asList("a","b","c");

        myList.stream().filter(s -> !s.equals("c"))
                .forEach(s -> System.out.println(s));
    }

}
